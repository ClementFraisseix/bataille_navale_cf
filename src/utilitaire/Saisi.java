package utilitaire;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Saisi {

	private Scanner sc;
	/**
	 * Permet de saisir un donn�e utilisateur
	 * @param p_texte = saisi utilisateur dans la console
	 * @return Le String que l'utilisateur a saisi
	 * @exception InputMismatchException si l'utilisateur saisi autre chose qu'un String
	 */
	public String SaisirTexte(String p_texte) {			
		String valeurEntree = "";

		try {
			// Cr�ation d'un objet Scanner permettant la saisie au clavier
			sc = new Scanner(System.in);

			// Affichage du texte
			System.out.print("\n" +p_texte);

			// Affichage du texte
			valeurEntree = sc.nextLine();

			//Catch de l'exception InputMismatchException au cas o�
			//autre chose qu'un String soit saisi
		}catch (InputMismatchException e) {
			System.err.println("\nVeuillez entrer une valeur valide: " + e);
		}

		// Retourner la valeur sous forme d'une String
		return valeurEntree;
	}
	/**
	 * Permet de saisir un donn�e utilisateur
	 * @param p_texte = saisi utilisateur dans la console
	 * @return Le int que l'utilisateur a saisi
	 * @exception InputMismatchException si l'utilisateur saisi autre chose qu'un int
	 */
	public int SaisirInt(String p_texte) {	
		int valeurEntree = 0;

		try {
			// Cr�ation d'un objet Scanner permettant la saisie au clavier
			sc = new Scanner(System.in);

			// Affichage du texte
			System.out.print("\n" +p_texte);

			// Affichage du texte
			valeurEntree = sc.nextInt();

		}catch (InputMismatchException e) {
			System.err.println("\nVeuillez entrer une valeur valide: " + e);
		}
		// Retourner la valeur sous forme d'un int
		return valeurEntree;
	}

	/**
	 * Permet de saisir un choix d'un utilisateur (O ou N, O = true, N = false).
	 * L'utilisateur rentre sa saisi sous forme de String et la m�thode retourne un bool�en
	 * @param p_texte = saisi utilisateur dans la console
	 * @return Un bool�en qui correspond au choix de l'utilisateur : oui = true, non = false.
	 */	
	public boolean SaisirUnChoix(String p_texte) {
		boolean v_choixbool = false;

		// Saisi utilisateur
		String v_choixentree = "";

		//Demander le choix de l'utilisateur tant qu'il n'a pas saisi un O ou un N, non sensitive case
		do {
			v_choixentree = SaisirTexte(p_texte);

			if(v_choixentree.equalsIgnoreCase("O"))
				v_choixbool = true;

			if(v_choixentree.equalsIgnoreCase("N"))
				v_choixbool = false;

			if(!v_choixentree.equalsIgnoreCase("O") && !v_choixentree.equalsIgnoreCase("N")) {
				System.err.println("\nVeuillez entrer une valeur valide");
				System.err.println("Saisir O ou N");
			}
		}while(!v_choixentree.equalsIgnoreCase("O") && !v_choixentree.equalsIgnoreCase("N"));

		return v_choixbool;
	}
	/**
	 * Permet de saisir le choix d'un bateau.
	 * L'utilisateur rentre sa saisi sous forme de String et la m�thode retourne un int
	 * @param p_texte = saisi utilisateur dans la console
	 * @return Un int qui correspond au bateau que l'utilisateur a choisi
	 */	
	public int SaisirUnBateau(String p_texte) {
		int v_choixbateau = 0;

		// Saisi utilisateur
		String v_choixentree = "";

		//Demander quel bateau l'utilisateur veut choisir tant que la saisi n'est pas valide, non sensitive case
		do {
			v_choixentree = SaisirTexte(p_texte);

			if(v_choixentree.equalsIgnoreCase("Croiseur"))
				v_choixbateau = 1; // Retourne 1 pour un croiseur

			if(v_choixentree.equalsIgnoreCase("Escorteur"))
				v_choixbateau = 2; // Retourne 2 pour un escorteur

			if(v_choixentree.equalsIgnoreCase("Porte avion"))
				v_choixbateau = 3; // Retourne 3 pour un porte avion
			
			if(v_choixentree.equalsIgnoreCase("Porte-avion"))
				v_choixbateau = 3; // Retourne 3 pour un porte avion

			if(v_choixentree.equalsIgnoreCase("Sous marin"))
				v_choixbateau = 4; // Retourne 3 pour un porte avion
			
			if(v_choixentree.equalsIgnoreCase("Sous-marin"))
				v_choixbateau = 4; // Retourne 3 pour un porte avion

			if(!v_choixentree.equalsIgnoreCase("Croiseur") && !v_choixentree.equalsIgnoreCase("Escorteur")
					&& !v_choixentree.equalsIgnoreCase("Porte avion")
					&& !v_choixentree.equalsIgnoreCase("Porte-avion")
					&& !v_choixentree.equalsIgnoreCase("Sous Marin")
					&& !v_choixentree.equalsIgnoreCase("Sous-Marin")) {
				System.err.println("\nVeuillez entrer une valeur valide");
				System.err.println("Croiseur");
				System.err.println("Escorteur");
				System.err.println("Porte avion");
				System.err.println("Sous Marin");
			}
		}while(!v_choixentree.equalsIgnoreCase("croiseur") 
				&& !v_choixentree.equalsIgnoreCase("escorteur") 
				&& !v_choixentree.equalsIgnoreCase("porte avion") 
				&& !v_choixentree.equalsIgnoreCase("porte-avion")
				&& !v_choixentree.equalsIgnoreCase("sous marin")
				&& !v_choixentree.equalsIgnoreCase("sous-marin"));

		return v_choixbateau;
	}
}
