package jeu;

import java.util.ArrayList;
import flotte.Bateau;
import flotte.Croiseur;
import flotte.Element;
import flotte.Escorteur;
import flotte.PorteAvion;
import flotte.SousMarin;
import utilitaire.Saisi;

public class Tableau {

	private Saisi sc = new Saisi();

	//Nombres de lignes et de colonnes de la grille
	private int NB_LIGNES;
	private int NB_COLONNES;

	//Les bateaux disponibles le joueur 1
	private Croiseur croiseurJ1;
	private Escorteur escorteurJ1;
	private PorteAvion porteAvionJ1;
	private SousMarin sousMarinJ1;

	//Les bateaux disponibles le joueur 2
	private Croiseur croiseurJ2;
	private Escorteur escorteurJ2;
	private PorteAvion porteAvionJ2;
	private SousMarin sousMarinJ2;

	//On met les bateaux de chaque joueurs dans leur listes respectives
	private ArrayList<Bateau> bateauxJ1;
	private ArrayList<Bateau> bateauxJ2;
	/**
	 * Création de la grille de jeu
	 * @param NB_LIGNES = nombres de lignes de la grille (abcisse)
	 * @param NB_COLONNES = nombres de colonnes de la grille (ordonnée)
	 */
	public Tableau (int NB_LIGNES, int NB_COLONNES){
		//Mémorisation du nb de lignes et de colonnes 
		//et de la listes de bateau des joueurs
		this.NB_LIGNES = NB_LIGNES +1;
		this.NB_COLONNES = NB_COLONNES +1;
		bateauxJ1 = new ArrayList<Bateau>();
		bateauxJ2 = new ArrayList<Bateau>();
	}
	/**
	 * Vérifie que le bateau n'est pas hors-grille
	 * @param bateau : le bateau que le joueur a choisi de positionner
	 * @param numJoueur : le joueur qui place son bateau (1 ou 2)
	 * @return true si le bateau est en dehors de la grille
	 */
	public boolean isBateauOut(Bateau bateau, int numJoueur){

		//Switch case pour effectuer le traitement suivant le joueur qui joue
		switch (numJoueur) {
		case 1:
			//Parcour les éléments du bateaux et vérifie que les éléments ne sont pas en dessous de 0 
			//ou au dessus du nombres de lignes et colonnes de la grille
			for (Element eb : bateau.getElements()) {
				if ((eb.getPositionX() <= 0) || (eb.getPositionX() >= NB_COLONNES)
						|| (eb.getPositionY() <=0 || eb.getPositionY() >= NB_LIGNES)){
					return true; //true quand le bateau est hors-grille
				}
			}
			break;//have a break, have a kitkat

			//Même chose mais pour le joueur 2
		case 2:
			for (Element eb : bateau.getElements()) {
				if ((eb.getPositionX() <= 0) || (eb.getPositionX() >= NB_COLONNES)
						|| (eb.getPositionY() <=0 || eb.getPositionY() >= NB_LIGNES)){
					return true;
				}
			}
			break;//Je préfère les berlines
		}
		return false;	
	}
	/**
	 * Ajoute un bateau à la liste du joueur correspondant
	 * @param bateau : le bateau que le joueur a choisi de positionner
	 * @param numJoueur : le joueur qui place son bateau (1 ou 2)
	 * @return true si le bateau a pu être ajouter à la liste du joueur
	 */
	public boolean ajouterBateau(Bateau bateau, int numJoueur) {

		//Vérifier que le bateau n'est pas superposé avec la méthode isBateauSuperpose
		//Vérifier que le bateau n'est pas hors-grille avec la méthode isBateauOut
		//Si les deux conditions sont vérifier, on ajoute le bateau à la liste correspondant au joueur
		switch (numJoueur) {
		case 1:
			if ((!isBateauSuperpose(bateau, 1)) && (!isBateauOut(bateau, 1)))
				bateauxJ1.add(bateau);
			else {
				System.err.println("Joueur 1 : Bateau invalide (Superposé à un autre, en dehors de la grille, ou coordonnées invalide)");
				return false;
			}
			break;//Je préfère les coupés
		case 2:
			if ((!isBateauSuperpose(bateau, 2)) && (!isBateauOut(bateau, 2)))
				bateauxJ2.add(bateau);
			else {
				System.err.println("Joueur 2 : Bateau invalide (Superposé à un autre, en dehors de la grille, ou coordonnées invalide)");
				return false;
			}
			break;//Voir les citadines
		}
		return true;	
	}
	/**
	 * Retire un bateau de la liste du joueur correspondant
	 * @param bateau : le bateau que le joueur a choisi de positionner
	 * @param numJoueur : le joueur qui place son bateau (1 ou 2)
	 * @return true si le bateau a bien été retirer
	 */
	public boolean retirerBateau(Bateau bateau, int numJoueur) {

		//Vérifie que le bateau passé en paramètres est bien 
		//dans la liste et le retire de celle-ci
		switch (numJoueur) {
		case 1:
			if(bateauxJ1.contains(bateau))
				bateauxJ1.remove(bateau);
			break;

		case 2:
			if(bateauxJ2.contains(bateau))
				bateauxJ2.remove(bateau);
			break;
		}
		return true;
	}
	/**
	 * Méthode qui permet de effectuer un tir sur un bateau adverse
	 * @param px = les coordonnées en x du tir effectué
	 * @param py = les coordonnées en y du tir effectué
	 * @param numJoueur = le joueur qui tir
	 * @return un int qui indique l'état du tir
	 */
	public int effectuerCoup(int px, int py, int numJoueur) {
		int resultat;

		//Vérifier que le tir est bien dans la grille
		if ((px <= 0) || (px >= NB_COLONNES) || (py <= 0) || (py >= NB_LIGNES)) {
			System.out.println("Coup en dehors du tableau");
			return 0; //Si le tir est hors-grille
		}
		else {
			//Switch case pour effectué le traitement en fonction du joueur qui effectue le tir
			//exemple : si le joueur 1 coule un bateau, le bateau correspondant est retiré de la liste du joueur 2
			switch (numJoueur) {
			case 1:
				//Parcours la liste de bateau du joueur 2
				for (Bateau bateau : bateauxJ2){
					//Vérifie l'état du bateau en appelant la méthode estTouche de la classe Bateau
					resultat = bateau.estTouche(px, py);
					if (resultat == 1)
						System.out.println("\nDéjà touché");
					else if (resultat == 2)
						System.out.println("\n" +bateau.toString()+" du joueur 2 touché");
					if (resultat != 0) {
						if (resultat == 3) {	
							//Si le bateau est coulé, on le retire de la liste du joueur correspondant
							retirerBateau(bateau, 2);
							System.out.println("\n" +bateau.toString()+ " du joueur 2 coulé");
						}
						return resultat;
					}
				}
				break;
				//Même chose mais si le joueur 2 effectue le tir
			case 2:
				for (Bateau bateau : bateauxJ1){
					resultat = bateau.estTouche(px, py);
					if (resultat == 1)
						System.out.println("\nDéjà touché");
					else if (resultat == 2)
						System.out.println("\n" + bateau.toString()+ " du joueur 1 touché");
					if (resultat != 0) {
						if (resultat == 3) {
							retirerBateau(bateau, 1);
							System.out.println("\n" +bateau.toString()+ " du joueur 1 coulé");
						}
						return resultat;
					}
				}
				break;
			}	
			System.out.println("\nCoup dans l'eau");	
			return 0;
		}
	}
	/**
	 * Affiche la grille de jeu
	 * @param numJoueur = le joueur pour lequel on veut afficher le plateau
	 * @return true si le plateau peut être affiché
	 */
	public boolean afficherPlateau (int numJoueur) { 
		//Vérifier que le nombre de ligne et de colonnes du plateau n'est pas inférieur à 0
		//Si la condition est respecté on affiche le plateau
		if (this.NB_COLONNES <= 10 || this.NB_LIGNES <= 10) {
			System.out.println("\nLe plateau doit faire au minimum 10 de côté");
			return false;
		}
		else {
			System.out.println("");

			for(int x1=0; x1 < NB_COLONNES; x1++) {
				System.out.print("|");
				if (x1<10)
					System.out.print(" ");
				System.out.print(""+x1+" ");
			}

			System.out.println("|");
			for (int y=1; y<NB_LIGNES; y++) {
				for (int y1=0; y1<NB_COLONNES; y1++) {
					System.out.print("----");
				}
				System.out.println("");
				if(y<10)
					System.out.print(" ");
				System.out.print(" "+y+" ");
				System.out.print("|");
				for (int x=1; x<NB_COLONNES; x++) {
					//Verifier que la case contient un élément en appelant la méthode containsBoat
					//Si la case ne contient pas d'élément on affiche une case vide,
					//Sinon on affiche un croix : X
					if(!containsBoat(x, y, numJoueur)) {
						System.out.print("   |");
					}
					else {
						System.out.print(" X |");
					}
				}
				System.out.println("");
			}
			for(int y1=0; y1<NB_COLONNES; y1++) {
				System.out.print("----");
			}
			return true;
		}
	}
	/**
	 * Vérifie qu'un élément est déjà positionné aux coordonnées passés en paramètres ou non
	 * @param x = coordonnée x qu'on veut vérifier
	 * @param y = coordonnée y qu'on veut vérifier
	 * @param numJoueur = le joueur pour lequel on veut vérifier les coordonnées
	 * @return false si les coordonnées ne contiennent pas de bateau
	 */
	public boolean containsBoat (int x, int y, int numJoueur) {
		//Switch case pour effectué le traitement en fonction du joueur
		switch (numJoueur) {
		case 1:
			//Parcours la liste du bateau du joueur
			for (Bateau b : bateauxJ1) {
				//Parcours les éléments de tous les bateaux de la liste du joueur
				for (Element e : b.getElements()) {
					//Si un éléments correspond à une coordonnées passé en paramètres
					//Alors les coordonnées contiennent un élément
					if (e.getPositionX() == x && e.getPositionY() == y) {
						return true;
					}
				}
			}
			break;
			//Même chose mais pour le joueur 2
		case 2:
			for (Bateau b : bateauxJ2) {
				for (Element e : b.getElements()) {
					if (e.getPositionX() == x && e.getPositionY() == y) {
						return true;
					}
				}
			}
			break;
		} 
		return false;	
	}
	/**
	 * Vérifie qu'un bateau n'est pas superposé à un autre, et qu'il n'est pas collé à un autre 
	 * @param bateau = bateau pour lequel on veut vérifier si il est superposé à un autre
	 * @param numJoueur = le joueur pour lequel on veut vérifier
	 * @return false si le bateau passé en paramètre n'est pas superposé et qu'il est séparé d'une case d'un autre bateau
	 */
	public boolean isBateauSuperpose(Bateau bateau, int numJoueur){
		//Parcours les deux collections de bateaux et pour chaque bateau
		//Vérifier que les coordonnées et que l'orientation n'est pas utilisé
		//Vérifier pour chaque élément de chaque bateau
		//On vérifie aussi que le bateau n'est pas collé à un autre, càd qu'au moins une case les séparent
		switch (numJoueur) {
		case 1:
			for (Bateau b : bateauxJ1) {
				for (Element e : b.getElements()) {
					for (Element eb : bateau.getElements()) {
						if (eb.getPositionX() == e.getPositionX() && eb.getPositionY() == e.getPositionY()
								|| (eb.getPositionX() == e.getPositionX()+1 && eb.getPositionY() == e.getPositionY()+1)
								|| (eb.getPositionX() == e.getPositionX()-1 && eb.getPositionY() == e.getPositionY()-1)){
							return true;
						}
					}
				}				
			}
			break;

		case 2:
			for (Bateau b : bateauxJ2) {
				for (Element e : b.getElements()) {
					for (Element eb : bateau.getElements()) {
						if (eb.getPositionX() == e.getPositionX() && eb.getPositionY() == e.getPositionY()
								|| (eb.getPositionX() == e.getPositionX()+1 && eb.getPositionY() == e.getPositionY()+1)
								|| (eb.getPositionX() == e.getPositionX()-1 && eb.getPositionY() == e.getPositionY()-1)){
							return true;
						}
					}
				}				
			}
			break;

		}
		return false;	
	}
	/**
	 * Permet de faire avancer un bateau aux joueurs 1
	 * @exception NullPointerException si le bateau que le joueur cherche à avancer a été coulé, et donc n'existe pas
	 */
	public void avancerBateauJ1() {
		try {
			//Appel de la méthode de saisi SaisirUnBateau qui renvoi en int le bateau qui correspond au choix du joueur
			//Appel de la fonction avancer de la classe Bateau en fonction du bateau choisi
			//Appel de la méthode de saisi SaisirUnInt pour saisir les coordonnées choisi par le joueur
			int choixBateau = sc.SaisirUnBateau("Quel bateau voulez-vous faire avancer joueur 1 ?"
					+ "\n Saisissez 'Croiseur' 'Escorteur' 'Porte avion' ou 'Sous Marin'");		
			if (choixBateau == 1) {
				croiseurJ1.avancer(sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe X ?"), 
						sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe Y ?"), 1);
				afficherPlateauJ1();
			}
			if (choixBateau == 2) {
				escorteurJ1.avancer(sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe X ?"), 
						sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe Y ?"), 1);
				afficherPlateauJ1();
			}
			if (choixBateau == 3) {
				porteAvionJ1.avancer(sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe X ?"), 
						sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe Y ?"), 1);
				afficherPlateauJ1();
			}
			if (choixBateau == 4) {
				sousMarinJ1.avancer(sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe X ?"), 
						sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe Y ?"), 1);
				afficherPlateauJ1();
			}
		}catch(NullPointerException e) {
			System.err.println("Le bateau n'existe pas: " + e);
		}
	}
	/**
	 * Permet de faire avancer un bateau aux joueurs 2
	 * @exception NullPointerException si le bateau que le joueur cherche à avancer a été coulé, et donc n'existe pas
	 */
	public void avancerBateauJ2() {
		try {
			//Appel de la méthode de saisi SaisirUnBateau qui renvoi en int le bateau qui correspond au choix du joueur
			//Appel de la fonction avancer de la classe Bateau en fonction du bateau choisi
			//Appel de la méthode de saisi SaisirUnInt pour saisir les coordonnées choisi par le joueur
			int choixBateau = sc.SaisirUnBateau("\nQuel bateau voulez-vous faire avancer joueur 2 ?"
					+ "\n Saisissez: 'Croiseur' 'Escorteur' 'Porte avion' ou 'Sous Marin'");
			if (choixBateau == 1) {
				croiseurJ2.avancer(sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe X ?"), 
						sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe Y ?"), 2);
				afficherPlateauJ2();
			}
			if (choixBateau == 2) {
				escorteurJ2.avancer(sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe X ?"), 
						sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe Y ?"), 2);
				afficherPlateauJ2();
			}
			if (choixBateau == 3) {
				porteAvionJ2.avancer(sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe X ?"), 
						sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe Y ?"), 2);
				afficherPlateauJ2();
			}
			if (choixBateau == 4) {
				sousMarinJ2.avancer(sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe X ?"), 
						sc.SaisirInt("De combien de case voulez-vous le faire avancer sur l'axe Y ?"), 2);
				afficherPlateauJ2();
			}
		}catch(NullPointerException e) {
			System.out.println("Le bateau n'existe pas: "+ e);
		}
	}
	/**
	 * Permet d'afficher la grille de jeu du joueurs 1
	 */
	public void afficherPlateauJ1() {
		System.out.println("\nPlateau du Joueur 1: ");
		afficherPlateau(1);
	}
	/**
	 * Permet d'afficher la grille de jeu du joueurs 2
	 */
	public void afficherPlateauJ2() {
		System.out.println("\nPlateau du Joueur 2: ");
		afficherPlateau(2);
	}
	/**
	 * Permet aux joueur 1 de faire plonger son sous-marin
	 * @exception NullPointerException si le sous-marin a déjà été coulé
	 */
	public void plongerSousMarinJ1() {
		try {
			//Booléen qui nous retourne le choix du joueur 1
			boolean choixPlongee = sc.SaisirUnChoix("Joueur 1 : Voulez-vous mettre votre sous-marin en plongée ? (O/N)");

			//Si choixPlongee est true alors on met le sous-marin en plongee
			if (choixPlongee == true) {
				sousMarinJ1.setbHorizontal(true);
				System.out.println("Le sous marin du joueur 1 est maintenant en plongée");
			}
			//Si choixPlongee est false alors on met le sous-marin en surface
			if (choixPlongee == false) {
				sousMarinJ1.setbHorizontal(false);		
				System.out.println("Le sous marin du joueur 1 est maintenant à la surface");
			}
		}catch(NullPointerException e) {
			System.out.println("Le sous-marin n'existe pas: "+ e);
		}
	}
	/**
	 * Permet aux joueur 2 de faire plonger son sous-marin
	 * @exception NullPointerException si le sous-marin a déjà été coulé
	 */
	public void plongerSousMarinJ2() {	
		try {
			//Booléen qui nous retourne le choix du joueur 1
			boolean choixPlongee = sc.SaisirUnChoix("Joueur 2 : Voulez-vous mettre votre sous-marin en plongée ? (O/N)");	

			//Si choixPlongee est true alors on met le sous-marin en plongee
			if (choixPlongee == true) {
				sousMarinJ2.setbHorizontal(true);
				System.out.println("Le sous marin du joueur 2 est maintenant en plongée");
			}
			//Si choixPlongee est false alors on met le sous-marin en surface
			if (choixPlongee == false) {
				sousMarinJ2.setbHorizontal(false);		
				System.out.println("Le sous marin du joueur 2 est maintenant à la surface");
			}
		}catch(NullPointerException e) {
			System.out.println("Le sous-marin n'existe pas: "+ e);
		}
	}
	/**
	 * Permet aux joueur 1 de placer ses bateaux
	 */
	public void placerBateauJ1() {	
		System.out.println("\nLe Joueur 1 place ses bateaux: ");
		//Booléen qui nous permet de savoir si l'ajout du bateau a été validé
		//On charge le résultat de ajouterBateau dans ce booléen
		boolean res;

		//On redemande l'ajout du bateau tant que res est false, càd
		//On redemande l'ajout du bateau au joueur tant que l'ajout du bateau n'est pas validé
		//Si l'ajout est valide, on affiche le plateau avec le bateau
		do {
			res = ajouterBateau(croiseurJ1 = new Croiseur(sc.SaisirInt("Position du Croiseur en X ? "), 
					sc.SaisirInt("Position du Croiseur en Y ? "), 
					sc.SaisirUnChoix("En position horizontal ? (O/N) "), "Croiseur"), 1);
		}while(res == false);
		afficherPlateauJ1();

		do {
			res = ajouterBateau(escorteurJ1 = new Escorteur(sc.SaisirInt("Position de l'Escorteur en X ? "), 
					sc.SaisirInt("Position de l'Escorteur en Y ? "), 
					sc.SaisirUnChoix("En position horizontal ? (O/N) "), "Escorteur"), 1);
		}while(res == false);
		afficherPlateauJ1();

		do {
			res = ajouterBateau(porteAvionJ1 = new PorteAvion(sc.SaisirInt("Position du Porte-Avion en X ? "), 
					sc.SaisirInt("Position du Porte-Avion en Y ? "), 
					sc.SaisirUnChoix("En position horizontal ? (O/N) "), "Porte-Avion"), 1);		
		}while(res == false);
		afficherPlateauJ1();

		do {
			res = ajouterBateau(sousMarinJ1 = new SousMarin(sc.SaisirInt("Position du Sous-Marin en X ? "), 
					sc.SaisirInt("Position du Sous-Marin en Y ? "), 
					sc.SaisirUnChoix("En plongée ? (O/N) "), "Sous-marin"), 1);			
		}while(res == false);
		afficherPlateauJ1();
	}
	/**
	 * Permet aux joueur 2 de placer ses bateaux
	 */
	public void placerBateauJ2() {	
		System.out.println("\nLe Joueur 2 place ses bateaux: ");
		//Booléen qui nous permet de savoir si l'ajout du bateau a été validé
		//On charge le résultat de ajouterBateau dans ce booléen
		boolean res;
		//On redemande l'ajout du bateau tant que res est false, càd
		//On redemande l'ajout du bateau au joueur tant que l'ajout du bateau n'est pas validé
		//Si l'ajout est valide, on affiche le plateau avec le bateau
		do {
			res = ajouterBateau(croiseurJ2 = new Croiseur(sc.SaisirInt("Position du Croiseur en X ? "), 
					sc.SaisirInt("Position du Croiseur en Y ? "), 
					sc.SaisirUnChoix("En position horizontal ? (O/N) "), "Croiseur"), 2);
		}while(res == false);
		afficherPlateauJ2();

		do {
			res = ajouterBateau(escorteurJ2 = new Escorteur(sc.SaisirInt("Position de l'Escorteur en X ? "), 
					sc.SaisirInt("Position de l'Escorteur en Y ? "), 
					sc.SaisirUnChoix("En position horizontal ? (O/N) "), "Escorteur"), 2);
		}while(res == false);
		afficherPlateauJ2();

		do {
			res = ajouterBateau(porteAvionJ2 = new PorteAvion(sc.SaisirInt("Position du Porte-Avion en X ? "), 
					sc.SaisirInt("Position du Porte-Avion en Y ? "), 
					sc.SaisirUnChoix("En position horizontal ? (O/N) "), "Porte-Avion"), 2);
		}while(res == false);
		afficherPlateauJ2();

		do {
			res = ajouterBateau(sousMarinJ2 = new SousMarin(sc.SaisirInt("Position du Sous-Marin en X ? "), 
					sc.SaisirInt("Position du Sous-Marin en Y ? "), 
					sc.SaisirUnChoix("En plongée ? (O/N) "), "Sous-Marin"), 2);	
		}while(res == false);
		afficherPlateauJ2();
	}
	public int getNB_LIGNES() {
		return NB_LIGNES;
	}
	public void setNB_LIGNES(int nB_LIGNES) {
		NB_LIGNES = nB_LIGNES;
	}
	public int getNB_COLONNES() {
		return NB_COLONNES;
	}
	public void setNB_COLONNES(int nB_COLONNES) {
		NB_COLONNES = nB_COLONNES;
	}
	public Croiseur getCroiseurJ1() {
		return croiseurJ1;
	}
	public void setCroiseurJ1(Croiseur croiseurJ1) {
		this.croiseurJ1 = croiseurJ1;
	}
	public Escorteur getEscorteurJ1() {
		return escorteurJ1;
	}
	public void setEscorteurJ1(Escorteur escorteurJ1) {
		this.escorteurJ1 = escorteurJ1;
	}
	public PorteAvion getPorteAvionJ1() {
		return porteAvionJ1;
	}
	public void setPorteAvionJ1(PorteAvion porteAvionJ1) {
		this.porteAvionJ1 = porteAvionJ1;
	}
	public SousMarin getSousMarinJ1() {
		return sousMarinJ1;
	}
	public void setSousMarinJ1(SousMarin sousMarinJ1) {
		this.sousMarinJ1 = sousMarinJ1;
	}
	public Croiseur getCroiseurJ2() {
		return croiseurJ2;
	}
	public void setCroiseurJ2(Croiseur croiseurJ2) {
		this.croiseurJ2 = croiseurJ2;
	}
	public Escorteur getEscorteurJ2() {
		return escorteurJ2;
	}
	public void setEscorteurJ2(Escorteur escorteurJ2) {
		this.escorteurJ2 = escorteurJ2;
	}
	public PorteAvion getPorteAvionJ2() {
		return porteAvionJ2;
	}
	public void setPorteAvionJ2(PorteAvion porteAvionJ2) {
		this.porteAvionJ2 = porteAvionJ2;
	}
	public SousMarin getSousMarinJ2() {
		return sousMarinJ2;
	}
	public void setSousMarinJ2(SousMarin sousMarinJ2) {
		this.sousMarinJ2 = sousMarinJ2;
	}
	public ArrayList<Bateau> getBateauxJ1() {
		return bateauxJ1;
	}
	public void setBateauxJ1(ArrayList<Bateau> bateauxJ1) {
		this.bateauxJ1 = bateauxJ1;
	}
	public ArrayList<Bateau> getBateauxJ2() {
		return bateauxJ2;
	}
	public void setBateauxJ2(ArrayList<Bateau> bateauxJ2) {
		this.bateauxJ2 = bateauxJ2;
	}
}