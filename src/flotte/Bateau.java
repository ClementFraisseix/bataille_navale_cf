package flotte;

public class Bateau {

	private int cpt = 0;
	private boolean bHorizontal;
	private boolean bTouche;

	private Element[] elements;

	public Bateau () {		
	}
	/**
	 * Cr�ation d'un bateau, on incr�mente de la taille du bateau en partant des coordonn�es du premier �l�ment
	 * @param x = position en x du 1er �l�ment
	 * @param y = position en y du 1er �l�ment
	 * @param bhonrizontale : = true : bateau positionner en horizontal sinon bateau positionner en vertical
	 * @param taille = nombre d'�l�ment du bateau
	 */
	protected Bateau(int x, int y, boolean bhorizontale, int taille) {
		this.bHorizontal=bhorizontale;
		this.bTouche=false;

		//Cr�ation d'un tableau d'�l�ment qui sera notre bateau
		elements = new Element[taille];
		for (int i=0; i < taille; i++) {
			if(bhorizontale)
				elements[i]=new Element(x+i, y, this);
			else
				elements[i]=new Element(x, y+i, this);
		}
	}
	/**
	 * @param px = la coordonn�e x qu'on v�rifie si touch�e ou pas
	 * @param py = la coordonn�e y qu'on v�rifie si touch�e ou pas
	 * @return un int qui correspond � l'�tat du bateau
	 * @see Element#estTouche(int, int)
	 */
	public int estTouche(int px, int py) {
		int res = 0;
		for(int i=0; i< elements.length; i++) {

			//Appel de la m�thode estTouche de la classe Element qui retourne l'�tat de l'�l�ment
			int resultat = elements[i].estTouche(px, py);	
			if(resultat==1) {
				res = 1; // = le bateau n'a pas �t� touch� 
				cpt++;
			}
			else if (resultat==2) {
				res = 2; // Le bateau a �t� touch�
				cpt++; // compteur pour savoir combien de fois le bateau a �t� touch�
			}
		}
		if (cpt == elements.length) {
			return 3; // Bateau coul�		
		}
		else
			return res;
	}
	/**
	 * M�thode qui fait avancer le bateau du nombre pass� en param�tre
	 * @param depx = le chiffre de combien le bateau doit avancer en x 
	 * @param depy = le chiffre de combien le bateau doit avancer en y
	 * @param numJoueur =  le joueur pour qui on avance le bateau
	 * @see Element#avancer(int, int)
	 */
	public void avancer (int depx, int depy, int numJoueur) { 

		//Switch case pour effectuer le traitement suivant le joueur qui joue
		switch(numJoueur) {
		case 1:
			//V�rification que le bateau n'avance pas ou ne recule pas de plus d'une case.
			if ((depx <=1 && depy <= 1) && (depx >=-1 && depy >=-1)){
				for(int i = 0; i<elements.length; i++)
					//Appel de la m�thode avancer de Element
					elements[i].avancer(depx, depy);	
			}
			else
				System.out.println("\nLe bateau ne peut se d�placer que d'une case");	
			//Le traitement a �t� fait, have a break, have a kitkat
			break;

			//M�me chose que pour la case 1 
		case 2:
			if ((depx <=1 && depy <= 1) && (depx >=-1 && depy >=-1)){
				for(int i = 0; i<elements.length; i++)
					elements[i].avancer(depx, depy);	
			}
			else
				System.out.println("\nLe bateau ne peut se d�placer que d'une case");	
			break;
		}
	}
	public boolean isbHorizontal() {
		return bHorizontal;
	}
	public void setbHorizontal(boolean bHorizontal) {
		this.bHorizontal = bHorizontal;
	}
	public boolean isbTouche() {
		return bTouche;
	}
	public void setbTouche(boolean bTouche) {
		this.bTouche = bTouche;
	}
	public Element[] getElements() {
		return elements;
	}
	public void setElements(Element[] elements) {
		this.elements = elements;
	}
}

