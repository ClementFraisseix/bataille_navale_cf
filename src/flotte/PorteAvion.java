package flotte;

public class PorteAvion extends Bateau {

	private static int NBELEMENTS = 5;
	private String name;
	/**
	 * Cr�ation d'un porte-avion
	 * @param x = position en x du 1er �l�ment
	 * @param y = position en y du 1er �l�ment
	 * @param bhonrizontale : = true : bateau positionner en horizontal sinon bateau positionner en vertical
	 * @param name = nom du bateau, ici le nom est "porte-avion"
	 */
	public PorteAvion (int x, int y, boolean bhonrizontale, String name){
		//Appel du constructeur de la classe m�re : Bateau
		super(x, y, bhonrizontale, NBELEMENTS);		
		this.name = name;
	}
	public static int getNBELEMENTS() {
		return NBELEMENTS;
	}
	public static void setNBELEMENTS(int nBELEMENTS) {
		NBELEMENTS = nBELEMENTS;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String toString() {
		return name;
	}
}
