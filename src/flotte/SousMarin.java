package flotte;

public class SousMarin extends Bateau{

	private static int NBELEMENTS = 1;
	private String name;
	private boolean enPlongee;
	/**
	 * Cr�ation d'un Sous-Marin
	 * @param x = position en x du 1er �l�ment
	 * @param y = position en y du 1er �l�ment
	 * @param bhonrizontale : = true : bateau positionner en horizontal sinon bateau positionner en vertical
	 * @param name = nom du bateau, ici le nom est "sous-marin"
	 */
	public SousMarin (int x, int y, boolean plongee, String name){
		//Appel du constructeur de la classe m�re : Bateau
		super(x, y, true, NBELEMENTS);
		this.name=name;
		this.enPlongee=plongee;
	}

	/**
	 * Red�finition de la m�thode "estTouche" de la classe Bateau
	 * @see Bateau#estTouche(int, int)
	 */
	public int estTouche(int px, int py){
		if (enPlongee)
			return 0; // Le bateau ne peut pas �tre touch� si enPlongee = true	
		else 
			//Sinon appel de la m�thode est touch� de la classe m�re
			return (super.estTouche(px,py));
	}

	/**
	 * Red�finition de la m�thode "avancer" de la classe Bateau
	 * @see Bateau#avancer(int, int, int)
	 */
	public void avancer (int depx, int depy, int numJoueur) {
		//Le sous marin ne peut pas avancer si il est en plong�e :
		//Si enPlongee = false, alors appel de la m�thode avancer de la classe m�re
		if(!enPlongee) {
			super.avancer(depx, depy, numJoueur);
		}
	}

	public static int getNBELEMENTS() {
		return NBELEMENTS;
	}

	public static void setNBELEMENTS(int nBELEMENTS) {
		NBELEMENTS = nBELEMENTS;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnPlongee() {
		return enPlongee;
	}

	public void setEnPlongee(boolean enPlongee) {
		this.enPlongee = enPlongee;
	}
	public String toString() {
		return name;
	}
}
