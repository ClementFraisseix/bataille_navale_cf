package flotte;

public class Element {

	private int positionX;
	private int positionY;	
	private boolean bTouche;	
	private Bateau bateau;
	/**
	 * Constructeur de la classe Element
	 * @param positionX = la position de l'�l�ment sur l'axe X
	 * @param positionY = la position de l'�l�ment sur l'axe Y
	 * @param bateau = Le bateau auquel appartient les �l�ments.
	 */
	public Element(int positionX, int positionY, Bateau bateau) {
		this.positionX = positionX;
		this.positionY = positionY;
		this.bateau = bateau;
		this.bTouche = false;
	}
	/**
	 * M�thode qui v�rifie si un �l�ment est touch� ou pas
	 * @param px = la coordonn�e x qu'on v�rifie si touch�e ou pas
	 * @param py = la coordonn�e y qu'on v�rifie si touch�e ou pas
	 * @return un int qui correspond � l'�tat de l'�l�ment
	 */
	public int estTouche(int px, int py) {
		if((positionX == px) && (positionY == py)){
			if (bTouche) {
				return 1; // Retourne 1 si l'�l�ment n'a pas �t� touch�
			}
			else {
				bTouche = true;
				return 2; // Retourne 2 si l'�l�ment a �t� touch�			
			}	
		} 
		else 
			return 0;
	}
	/**
	 * M�thode qui fait avancer l'�l�ment du nombre pass� en param�tre
	 * @param depx = le chiffre de combien l'�l�ment doit avancer en x 
	 * @param depy = le chiffre de combien l'�l�ment doit avancer en y
	 */
	public void avancer(int depx, int depy) {
		//On additionne � la position d�j� connu, le chiffre pass� en param�tre
		positionX = positionX + depx;
		positionY = positionY + depy;
	}
	public int getPositionX() {
		return positionX;
	}
	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}
	public int getPositionY() {
		return positionY;
	}
	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
	public boolean isbTouche() {
		return bTouche;
	}
	public void setbTouche(boolean bTouche) {
		this.bTouche = bTouche;
	}
	public Bateau getBateau() {
		return bateau;
	}
	public void setBateau(Bateau bateau) {
		this.bateau = bateau;
	}
}
