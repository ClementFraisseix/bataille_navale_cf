package principal; 

import jeu.Tableau;
import utilitaire.Saisi;

public class Lanceur {	

	public static void main(String[] args) {

		boolean pasGagne = true;
		boolean rejouer = true;

		Saisi sc = new Saisi();
		//On demande aux joueurs la taille d�sir� de la grille de jeu directement � l'instaciation de celui-ci
		//A l'aide du constructeur de la classe Tableau
		Tableau plateau = new Tableau(sc.SaisirInt("Combien la grille doit comporter de lignes? "),
				sc.SaisirInt("Combien la grille doit comporter de colonnes? "));

		//Tant que la grille ne respecte pas les r�gles du jeu 
		//(au moins 10 de cot�), on redemande la taille du tableau en rappellant le lanceur lui-m�me
		//Vu que c'est le premier traitement du main
		while(plateau.afficherPlateau(0) == false) {
			Lanceur.main(args);
		}

		System.out.println("\nQue la bataille commence !");
		//Appel des m�thodes placerBateauJ1 et J2 afin que les joueurs placent leur bateaux sur la grille
		plateau.placerBateauJ1();
		plateau.placerBateauJ2();
		//Une fois les bateaux plac�s, on affiche le plateau de chacun des joueurs
		plateau.afficherPlateauJ1();
		plateau.afficherPlateauJ2();

		//Boucle de jeu :
		//Tant que pasGagne = true, le joue continue
		//pasGagne passe = false si un des joueurs n'a plus de bateau dans sa liste
		while (pasGagne) {
			System.out.println("\nLe joueur 1 tir !");
			plateau.effectuerCoup(sc.SaisirInt("Tir en X ? "), sc.SaisirInt("Tir en Y ? "), 1);

			if (plateau.getBateauxJ2().isEmpty()) {
				System.out.println("Joueur 1 est le gagnant !");
				pasGagne = false;
			}
			else {
				System.out.println("\nLe joueur 2 tir !");
				plateau.effectuerCoup(sc.SaisirInt("Tir en X ? "), sc.SaisirInt("Tir en Y ? "), 2);	

				if (plateau.getBateauxJ1().isEmpty()) {
					System.out.println("Joueur 2 est le gagnant !");
					pasGagne = false;
				}		
			}
			//Demande d'�volution du jeu : 
			//Demande si les joueurs veulent avancer leurs bateaux
			//Demande si les joueurs veulent mettre sur sous-marin respectifs en plong�e ou pas
			if (pasGagne) {
				boolean choixAvancerJ1 = sc.SaisirUnChoix("\nJoueur 1 : Voulez-vous faire avancer un bateau? (O/N)");		
				if (choixAvancerJ1) {
					plateau.avancerBateauJ1();			
				}
				boolean choixAvancerJ2 = sc.SaisirUnChoix("\nJoueur 2 : Voulez-vous faire avancer un bateau? (O/N)");		
				if (choixAvancerJ2) {
					plateau.avancerBateauJ2();			
				}

				//Si le dernier bateaux des joueurs est un sous-marin, alors le sous-marin du joueurs
				//Correspondant reste en surface afin que le jeu puisse se terminer car
				//Les sous-marins ne peuvent pas �tre toucher en plong�e			
				if (plateau.getBateauxJ1().size() == 1 && plateau.getBateauxJ1().contains(plateau.getSousMarinJ1())){
					plateau.getSousMarinJ2().setEnPlongee(false);
					System.out.println("\nLe joueur 1 n'a plus qu'un sous-marin sur sa grille, il reste donc en surface � partir de maintenant !");
				}
				else {
					plateau.plongerSousMarinJ1();
				}

				if (plateau.getBateauxJ2().size() == 1 && plateau.getBateauxJ2().contains(plateau.getSousMarinJ2())){
					plateau.getSousMarinJ2().setEnPlongee(false);
					System.out.println("\nLe joueur 2 n'a plus qu'un sous-marin sur sa grille, il reste donc en surface � partir de maintenant !");

				}
				else {
					plateau.plongerSousMarinJ2();
				}
			}
		}

		//Demander aux joueurs si il veulent rejouer, si oui, on relance le lanceur lui m�me
		//Sinon on fait une r�f�rences aux guignols de l'info
		rejouer = sc.SaisirUnChoix("Voulez-vous rejouer? (O/N) ");	
		if (rejouer)
			Lanceur.main(args);
		else 
			System.out.println("\nA tchao bonsoir!");
	}
}
